class Auto:
    def __init__(self, farbe):
        self.__Farbe = farbe
        self.__Fahrer = None

    def getfarbe(self):
        return self.__Farbe

    def getfahrer(self):
        return self.__Fahrer

    def setfahrer(self, fahrer):
        self.__Fahrer = fahrer

    def lackieren(self,farbe):
        if farbe in ["Rot", "Grün", "Blau", "Gelb"]:
            self.__Farbe = farbe
        else:
            print("Ungültige Farbe!")


class Fahrer:
    def __init__(self, name):
        self.__Name = name

    def getname(self):
        return self.__Name


class Verwaltung:
    def __init__(self):
        self.__Autos = []

    def autohinzufügen(self, auto):
        self.__Autos.append(auto)

    def fahrerhinzufügen(self, index, fahrer):
        self.__Autos[index].setfahrer(fahrer)


    def getautos(self):
        return self.__Autos


if __name__ == "__main__":
    fahrer1 = Fahrer("Meier")
    auto1 = Auto("Rot")
    auto2 = Auto("Gelb")
    auto3 = Auto("Blau")

    v1 = Verwaltung()
    v1.autohinzufügen(auto1)
    v1.autohinzufügen(auto2)
    v1.autohinzufügen(auto3)

    v1.fahrerhinzufügen(0,fahrer1)

    print(v1.getautos()[0].getfahrer().getname())
    print(v1.getautos()[1].getfarbe())

