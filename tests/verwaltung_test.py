import unittest
from src.verwaltung import Fahrer, Auto

class AutoTest(unittest.TestCase):
    def setUp(self):
        self.fahrer = Fahrer("Scholz")
        self.auto = Auto("Rot")
        self.auto.setfahrer(self.fahrer)

    def testLackieren(self):
        self.auto.lackieren("Gelb")
        self.assertEqual("Gelb", self.auto.getfarbe())

    def testUngueltigeFarbe(self):
        self.auto.lackieren("Pink")
        self.assertEqual("Rot", self.auto.getfarbe())
